import { ClassComponent } from "./components/ClassComponent";
import { CreateReactElement } from "./components/CreateReactElement";
import { FuncComponent } from "./components/FuncComponent";
import JsxComponent from "./components/JsxComponent";


function App() {
  return (
    <div className="App">
        {/* <h1>Samariddin Nurmamatov</h1> */}
        <CreateReactElement />   
        <JsxComponent />  
        <FuncComponent/>   
        <ClassComponent />
    </div>
  );
}

export default App;
